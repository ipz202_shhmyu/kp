﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Сourse.Object;

namespace Сourse
{
    public partial class Comparing : Form
    {
        Planet first;
        Planet second;
        public Comparing()
        {
            InitializeComponent();
        }
        public Comparing(Planet a, Planet b)
        {
            InitializeComponent();
            first = a;
            second = b;
            f.Image = first.BackgroundImage;
            f.SizeMode = PictureBoxSizeMode.CenterImage;
            s.Image = second.BackgroundImage;
            s.SizeMode = PictureBoxSizeMode.CenterImage;
        }
        

        private void Compare_Load(object sender, EventArgs e)
        {
            f.BackColor = Color.Transparent;
            s.BackColor = Color.Transparent;
            label1.Text = first.Info;
            label2.Text = second.Info;
        }
    }
}

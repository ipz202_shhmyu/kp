﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Сourse.Object;

namespace Сourse
{
    public partial class MainForm : Form
    {
        
        public MainForm()
        {
            Point center;
            InitializeComponent();
            center = new Point(300 + 51, 300 + 71);
            sun.SetPlanet(1, 0, 0, center, 5000, "Назва: Сонце\nТип: Зірка\nДіаметр: 1.4 млн. км\nТемпература поверхні: 5 778 К\nМаса: 1,989E30 кг");
            center = new Point(330 + 51, 330 + 71);
            merk.SetPlanet(2, 20, 50, center, 50, "Назва: Меркурій\nТип: планета земної групи\nДіаметр: 4 800 км\nТемпература поверхні: 80-700 К\nМаса: 3,285E23 кг");
            center = new Point(320 + 51, 320 + 71);
            venera.SetPlanet(3, 20, 80, center, 10, "Назва: Венера\nТип: планета земної групи\nДіаметр: 12 100 км\nТемпература поверхні: 737 К\nМаса: 4,867E24 кг");
            earth.SetPlanet(4, 10, 120, center, 20, "Назва: Земля\nТип: планета земної групи\nДіаметр: 12 700 км\nТемпература поверхні: 287,2 К\nМаса: 5,972E24 кг");
            center = new Point(325 + 51, 325 + 71);
            mars.SetPlanet(5, 6, 170, center, 0, "Назва: Марс\nТип: планета земної групи\nДіаметр: 6 800 км\nТемпература поверхні: 210 K\nМаса: 6,39E23 кг");
            center = new Point(309 + 51, 309 + 71);
            jupiter.SetPlanet(6, 8, 270, center, 0, "Назва: Юпітер\nТип: газова планета-гігант\nДіаметр: 140 000 км\nТемпература поверхні(!): 128 К\nМаса: 1,898E27 кг");
            center = new Point(309 + 51, 309 + 71);
            saturn.SetPlanet(7, 9, 360, center, 30, "Назва: Сатурн\nТип: газова планета-гігант\nДіаметр:116 460 км\nТемпература поверхні(!): 134 К\nМаса: 5,683E26 кг");

        }

        private void Form1_Load(object sender, EventArgs e)
        {
           
        }


        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            Pen myPen = new Pen(Color.White, 1);
            Graphics g = e.Graphics;
            g.DrawEllipse(myPen, 160 + 51, 160 + 71, 2 * 170, 2 * 170);
            g.DrawEllipse(myPen, 286 + 51, 286 + 71, 2 * 50, 2 * 50);
            g.DrawEllipse(myPen, 256 + 51, 256 + 71, 160, 160);
            g.DrawEllipse(myPen, 216 + 51, 216 + 71, 240, 240);
            g.DrawEllipse(myPen, 60 + 51, 60 + 71, 540, 540);
            g.DrawEllipse(myPen, 23, 43, 720, 720);
        }

        private void comporator_Click(object sender, EventArgs e)
        {
            Planet tmp = sun, tmp1=jupiter;
            if (sun.ID == WorldInfo.checked1)
            {
                tmp = sun;
            }
            else if (merk.ID == WorldInfo.checked1)
            {
                tmp = merk;
            }
            else if (venera.ID == WorldInfo.checked1)
            {
                tmp = venera;
            }
            else if (earth.ID == WorldInfo.checked1)
            {
                tmp = earth;
            }
            else if (mars.ID == WorldInfo.checked1)
            {
                tmp = mars;
            }
            else if (jupiter.ID == WorldInfo.checked1)
            {
                tmp = jupiter;
            }
            else if (saturn.ID == WorldInfo.checked1)
            {
                tmp = saturn;
            }

            if (sun.ID == WorldInfo.checked2)
            {
                tmp1 = sun;
            }
            else if (merk.ID == WorldInfo.checked2)
            {
                tmp1 = merk;
            }
            else if (venera.ID == WorldInfo.checked2)
            {
                tmp1 = venera;
            }
            else if (earth.ID == WorldInfo.checked2)
            {
                tmp1 = earth;
            }
            else if (mars.ID == WorldInfo.checked2)
            {
                tmp1 = mars;
            }
            else if (jupiter.ID == WorldInfo.checked2)
            {
                tmp1 = jupiter;
            }
            else if (saturn.ID == WorldInfo.checked2)
            {
                tmp1 = saturn;
            }

            Comparing childForm = new Comparing(tmp,tmp1);        
            childForm.Show();
        }

        private void pause_Click(object sender, EventArgs e)
        {
            if(WorldInfo.IsPaused)
            WorldInfo.IsPaused = false;
            else
            {
                WorldInfo.IsPaused = true;
            }
        }

        private void timerUpdate_Tick(object sender, EventArgs e)
        {
            if (WorldInfo.checked1 != 0 && WorldInfo.checked2 != 0)
            {
                comporator.Enabled = true;
            }
            else
                comporator.Enabled = false;
        }
    }
}

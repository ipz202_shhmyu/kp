﻿namespace Сourse
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.timerUpdate = new System.Windows.Forms.Timer(this.components);
            this.comporator = new System.Windows.Forms.Button();
            this.pause = new System.Windows.Forms.Button();
            this.saturn = new Сourse.Object.Planet();
            this.jupiter = new Сourse.Object.Planet();
            this.mars = new Сourse.Object.Planet();
            this.earth = new Сourse.Object.Planet();
            this.venera = new Сourse.Object.Planet();
            this.merk = new Сourse.Object.Planet();
            this.sun = new Сourse.Object.Planet();
            ((System.ComponentModel.ISupportInitialize)(this.saturn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.jupiter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mars)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.earth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.venera)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.merk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sun)).BeginInit();
            this.SuspendLayout();
            // 
            // timerUpdate
            // 
            this.timerUpdate.Enabled = true;
            this.timerUpdate.Interval = 10;
            this.timerUpdate.Tick += new System.EventHandler(this.timerUpdate_Tick);
            // 
            // comporator
            // 
            this.comporator.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.comporator.Enabled = false;
            this.comporator.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.comporator.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.comporator.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray;
            this.comporator.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.comporator.Font = new System.Drawing.Font("Nirmala UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comporator.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.comporator.Location = new System.Drawing.Point(894, 12);
            this.comporator.Name = "comporator";
            this.comporator.Size = new System.Drawing.Size(118, 42);
            this.comporator.TabIndex = 7;
            this.comporator.Text = "COMPARE";
            this.comporator.UseVisualStyleBackColor = false;
            this.comporator.Click += new System.EventHandler(this.comporator_Click);
            // 
            // pause
            // 
            this.pause.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.pause.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.pause.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.pause.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray;
            this.pause.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.pause.Font = new System.Drawing.Font("Nirmala UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pause.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.pause.Location = new System.Drawing.Point(758, 12);
            this.pause.Name = "pause";
            this.pause.Size = new System.Drawing.Size(118, 42);
            this.pause.TabIndex = 8;
            this.pause.Text = "PAUSE";
            this.pause.UseVisualStyleBackColor = false;
            this.pause.Click += new System.EventHandler(this.pause_Click);
            // 
            // saturn
            // 
            this.saturn.BackColor = System.Drawing.Color.Transparent;
            this.saturn.BackgroundImage = global::Сourse.Properties.Resources.saturn67x38;
            this.saturn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.saturn.Location = new System.Drawing.Point(0, 0);
            this.saturn.Name = "saturn";
            this.saturn.Size = new System.Drawing.Size(80, 50);
            this.saturn.TabIndex = 6;
            this.saturn.TabStop = false;
            this.saturn.Tag = System.Drawing.Color.Transparent;
            // 
            // jupiter
            // 
            this.jupiter.BackColor = System.Drawing.Color.Transparent;
            this.jupiter.BackgroundImage = global::Сourse.Properties.Resources.Jupiter50x50;
            this.jupiter.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.jupiter.Location = new System.Drawing.Point(0, 0);
            this.jupiter.Name = "jupiter";
            this.jupiter.Size = new System.Drawing.Size(70, 70);
            this.jupiter.TabIndex = 5;
            this.jupiter.TabStop = false;
            this.jupiter.Tag = System.Drawing.Color.Transparent;
            // 
            // mars
            // 
            this.mars.BackColor = System.Drawing.Color.Transparent;
            this.mars.BackgroundImage = global::Сourse.Properties.Resources.mars12x12;
            this.mars.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.mars.Location = new System.Drawing.Point(0, 0);
            this.mars.Name = "mars";
            this.mars.Size = new System.Drawing.Size(20, 20);
            this.mars.TabIndex = 4;
            this.mars.TabStop = false;
            // 
            // earth
            // 
            this.earth.BackColor = System.Drawing.Color.Transparent;
            this.earth.BackgroundImage = global::Сourse.Properties.Resources.Earth25x25;
            this.earth.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.earth.Location = new System.Drawing.Point(0, 0);
            this.earth.Name = "earth";
            this.earth.Size = new System.Drawing.Size(40, 40);
            this.earth.TabIndex = 3;
            this.earth.TabStop = false;
            // 
            // venera
            // 
            this.venera.BackColor = System.Drawing.Color.Transparent;
            this.venera.BackgroundImage = global::Сourse.Properties.Resources.Venera20x20;
            this.venera.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.venera.Location = new System.Drawing.Point(0, 0);
            this.venera.Name = "venera";
            this.venera.Size = new System.Drawing.Size(35, 35);
            this.venera.TabIndex = 2;
            this.venera.TabStop = false;
            // 
            // merk
            // 
            this.merk.BackColor = System.Drawing.Color.Transparent;
            this.merk.BackgroundImage = global::Сourse.Properties.Resources.merkurii10x10;
            this.merk.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.merk.Location = new System.Drawing.Point(0, 0);
            this.merk.Name = "merk";
            this.merk.Size = new System.Drawing.Size(20, 20);
            this.merk.TabIndex = 1;
            this.merk.TabStop = false;
            // 
            // sun
            // 
            this.sun.BackColor = System.Drawing.Color.Transparent;
            this.sun.BackgroundImage = global::Сourse.Properties.Resources.Sun72x72;
            this.sun.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.sun.Location = new System.Drawing.Point(0, 0);
            this.sun.Margin = new System.Windows.Forms.Padding(4);
            this.sun.Name = "sun";
            this.sun.Size = new System.Drawing.Size(98, 90);
            this.sun.TabIndex = 0;
            this.sun.TabStop = false;
            this.sun.Tag = System.Drawing.Color.Transparent;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1024, 995);
            this.Controls.Add(this.pause);
            this.Controls.Add(this.comporator);
            this.Controls.Add(this.saturn);
            this.Controls.Add(this.jupiter);
            this.Controls.Add(this.mars);
            this.Controls.Add(this.earth);
            this.Controls.Add(this.venera);
            this.Controls.Add(this.merk);
            this.Controls.Add(this.sun);
            this.Cursor = System.Windows.Forms.Cursors.Hand;
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximumSize = new System.Drawing.Size(1042, 1042);
            this.MinimumSize = new System.Drawing.Size(1042, 1042);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Модель Сонячної Системи";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Form1_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.saturn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.jupiter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mars)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.earth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.venera)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.merk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sun)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Object.Planet sun;
        private System.Windows.Forms.Timer timerUpdate;
        private Object.Planet merk;
        private Object.Planet venera;
        private Object.Planet earth;
        private Object.Planet mars;
        private Object.Planet jupiter;
        private Object.Planet saturn;
        private System.Windows.Forms.Button comporator;
        private System.Windows.Forms.Button pause;
    }
}
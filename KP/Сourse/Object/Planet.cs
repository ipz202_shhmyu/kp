﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Сourse.Object
{
    public class Planet : PictureBox
    {
        private int year = 0;
        private int id;
        private string info;
        private double speed;
        private double r;
        private Point center;
        private int counter = 1;
        private bool left=true;      
        private System.Windows.Forms.Timer Move;
        private System.ComponentModel.IContainer components;
      
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.Move = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // Move
            // 
            this.Move.Enabled = true;
            this.Move.Interval = 10;
            this.Move.Tick += new System.EventHandler(this.Move_Tick);
            // 
            // Planet
            // 
            this.BackColor = System.Drawing.Color.Transparent;
            this.BackgroundImage = global::Сourse.Properties.Resources.Sun72x72;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.Size = new System.Drawing.Size(100, 100);
            this.Click += new System.EventHandler(this.Planet_Click);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Planet_Paint);
            this.DoubleClick += new System.EventHandler(this.Planet_DoubleClick);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }
        public Planet()
        {
            InitializeComponent();
        }
   
        public void SetPlanet(int i,double a, double b, Point c,int d, string e) 
        {
            speed = a;
            r = b;
            center = c;
            counter = d;
            info = e;
            id = i;
        }
        private void Move_Tick(object sender, EventArgs e)
        {
            if (!WorldInfo.IsPaused)
            {
                int X = center.X + (int)Math.Round(r * Math.Cos(counter * 0.01 * speed));
                int Y = center.Y + (int)Math.Round(r * Math.Sin(counter * 0.01 * speed));

                if (X < center.X)
                {
                    left = true;
                }
                else if (X > center.X && left)
                {
                    left = false;
                    year++;
                }

                counter++;
                Location = new Point(X, Y);
            }
            
        }
        public int ID
        {
            get
            {
                return id;
            }

            
        }
        public string Info
        {
            get
            {
                return info + "\nРік: " + year;
            }


        }
        private void Planet_Click(object sender, EventArgs e)
        {
            
            
            if ((Color)this.Tag == Color.Transparent) 
            { 
                
                if (WorldInfo.checked1 == 0)
                {
                    WorldInfo.checked1 = id;
                    this.Tag = Color.Blue;
                    
                }
                else if (WorldInfo.checked2 == 0)
                {
                    WorldInfo.checked2 = id;
                    this.Tag = Color.Blue;
                    
                }
            }
            else { this.Tag = Color.Transparent; 
                
                    Move.Start();
                    if (WorldInfo.checked1 == id)
                    {
                        WorldInfo.checked1 = 0;
                       
                    }
                    else if (WorldInfo.checked2 == id)
                    {
                        WorldInfo.checked2 = 0;
                      
                    }
                
            }
            this.Refresh();
        }

     

      

        private void Planet_DoubleClick(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show(info+"\nРік: "+year, "Інформація", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void Planet_Paint(object sender, PaintEventArgs e)
        {
           
            if (this.Tag == null) { this.Tag = Color.Transparent;  }
            ControlPaint.DrawBorder(e.Graphics, this.ClientRectangle, (Color)this.Tag, ButtonBorderStyle.Solid);
        }
    }
    
}
